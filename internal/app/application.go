package app

import (
	"log"
	"os"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.gnome.org/csoriano/weeport/internal/i18n"
)

// WeePort is the application structure
type WeePort struct {
	GtkObject *gtk.Application
	window    *mainWindow

	debug       bool
	errorFormat string
}

// New instantiates a new WeePort application
func New(options ...WeePortOption) *WeePort {
	gtkObj, err := gtk.ApplicationNew("io.csoriano.Weeport",
		glib.APPLICATION_FLAGS_NONE)
	if err != nil {
		log.Fatalf("couldn't create GTKApplication: %v", err)
	}
	app := WeePort{GtkObject: gtkObj}

	for _, option := range options {
		option(&app)
	}

	log.SetFlags(0)
	if app.debug {
		log.SetFlags(log.Lshortfile)
		log.Println(i18n.G("DEBUG enabled"))
		app.errorFormat = "%+v"
	}

	app.GtkObject.Connect("activate", app.onActivate)

	return &app
}

// WeePortOption are official way of customizing the WeePort apps on creation
type WeePortOption func(*WeePort)

// WithDebug toggles debugging on main application
func WithDebug(app *WeePort) {
	app.debug = true
}

func (app *WeePort) onActivate() {
	errorFormat := app.errorFormat
	if errorFormat == "" {
		errorFormat = "%v"
	}
	w, err := newMainWindow(app.debug, errorFormat)
	if err != nil {
		log.Printf("ERROR: can't create main ui window: "+errorFormat, err)
		os.Exit(1)
	}
	app.GtkObject.AddWindow(&w.GtkObject.Window)
}
