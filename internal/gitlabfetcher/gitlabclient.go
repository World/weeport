package gitlabfetcher

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
	"time"

	"gitlab.gnome.org/csoriano/weeport/internal/contrib"
	"golang.org/x/xerrors"

	"github.com/gotk3/gotk3/glib"
	gitlab "github.com/xanzy/go-gitlab"
)

const gitlabGNOMEURL = "https://gitlab.gnome.org/api/v4"

var (
	gitlabClient  *gitlab.Client
	currentUserID = -1
	once          sync.Once
)

func newGitlabClient() (*gitlab.Client, error) {
	b, err := ioutil.ReadFile(getTokenFilePath())
	if err != nil {
		return nil, xerrors.Errorf("couldn't open token file: %v", err)
	}
	var giterr error
	once.Do(func() {
		token := string(b)
		git := gitlab.NewClient(nil, token)
		git.SetBaseURL(gitlabGNOMEURL)
		gitlabClient = git
		user, _, err := git.Users.CurrentUser()
		if err != nil {
			giterr = err
			return
		}
		currentUserID = user.ID
	})
	if giterr != nil {
		return nil, xerrors.Errorf("couldn't get gitlab user: %v", giterr)
	}
	return gitlabClient, nil
}

// SaveToken records gitlab token to disk
func SaveToken(token string) error {
	p := getTokenFilePath()
	if err := os.MkdirAll(filepath.Dir(p), os.ModePerm); err != nil {
		return xerrors.Errorf("couldn't create data path directory: %v", err)
	}

	if err := ioutil.WriteFile(p, []byte(token), 0666); err != nil {
		return xerrors.Errorf("couldn't write to token to file: %v", err)
	}
	return nil
}

// CheckToken tests if token file exists and is readable
func CheckToken() bool {
	_, err := ioutil.ReadFile(getTokenFilePath())
	return err == nil
}

func getTokenFilePath() string {
	dataPath := filepath.Join(glib.GetUserDataDir(), "weeport")
	return filepath.Join(dataPath, "gitlabToken.txt")
}

// DownloadProgressData represents current status over max expected one.
type DownloadProgressData struct {
	Current int
	Max     int
}

// GNOMEContributions returns all contributions for current gitlab token
func GNOMEContributions(progress chan<- DownloadProgressData) ([]contrib.Contribution, error) {
	gitlabClient, err := newGitlabClient()
	if err != nil {
		return nil, err
	}
	lastWeek := time.Now().Add(-7 * 24 * time.Hour)
	from := gitlab.ISOTime(lastWeek)
	opts := gitlab.ListContributionEventsOptions{
		After: &from,
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}
	_, res, err := gitlabClient.Events.ListCurrentUserContributionEvents(&opts)
	if err != nil {
		return nil, xerrors.Errorf("couldn't get list of current user contributions: %v", err)
	}
	var result []contrib.Contribution
	for page := 1; page <= res.TotalPages; page++ {
		opts.Page = page
		contributions, _, err := gitlabClient.Events.ListCurrentUserContributionEvents(&opts)
		if err != nil {
			return nil, xerrors.Errorf("couldn't get list of current user contributions: %v", err)
		}
		var action string
		for i, cont := range contributions {
			// only send to channel if specified (still allow sync calls)
			if progress != nil {
				progress <- DownloadProgressData{(i + 1) * (page), res.TotalItems}
			}
			var link, description string
			var mrAction MRAction
			var issueAction IssueAction
			var iid, authorID int
			GLProject, _, err := gitlabClient.Projects.GetProject(cont.ProjectID, nil)
			if err != nil {
				return nil, xerrors.Errorf("couldn't get list of current user contributions: %v", err)
			}
			project := GLProject.Name
			target := cont.TargetType
			if cont.TargetType == "MergeRequest" {
				mrOpts := gitlab.GetMergeRequestsOptions{}
				mr, _, err := gitlabClient.MergeRequests.GetMergeRequest(cont.ProjectID, cont.TargetIID, &mrOpts)
				if err != nil {
					return nil, xerrors.Errorf("couldn't get list of current user merge request: %v", err)
				}
				link = mr.WebURL
				description = mr.Description
				iid = mr.IID
				authorID = mr.Author.ID
				mrAction, err = getMRText(cont.ActionName, authorID)
				action = string(mrAction)
				if err != nil {
					return nil, xerrors.Errorf("Couldn't recognize merge request text: %v", err)
				}
			} else if cont.TargetType == "Issue" {
				issue, _, err := gitlabClient.Issues.GetIssue(cont.ProjectID, cont.TargetIID, nil)
				if err != nil {
					return nil, xerrors.Errorf("couldn't get list of current user issues contributions: %v", err)
				}
				link = issue.WebURL
				description = issue.Description
				iid = issue.IID
				authorID = issue.Author.ID
				issueAction, err = getIssueText(cont.ActionName, authorID)
				action = string(issueAction)
				if err != nil {
					return nil, xerrors.Errorf("Couldn't recognize issue text: %v", err)
				}
			} else {
				// Don't save unhandled data
				continue
			}

			c := contrib.Contribution{
				Title:       cont.TargetTitle,
				Link:        link,
				Description: description,
				ID:          iid,
				AuthorID:    authorID,

				GLProjectName: GLProject.Name,
				GLTargetType:  cont.TargetType,
				GLActionName:  cont.ActionName,
				GLCreatedAt:   *cont.CreatedAt,

				ProjectName: project,
				TargetType:  target,
				ActionName:  action,
			}
			result = append(result, c)
		}
	}
	return result, nil
}

type MRAction string

const (
	ReviewedAndMerged    MRAction = "Reviewed and Merged"
	ImplementedAndMerged MRAction = "Implemented and Merged"
	Implemented          MRAction = "Implemented"
	Rejected             MRAction = "Rejected"
)

type IssueAction string

const (
	Opened      IssueAction = "Opened"
	Closed      IssueAction = "Closed"
	CommentedOn IssueAction = "Commented On"
)

func getIssueText(actionNameGL string, authorID int) (IssueAction, error) {
	switch actionNameGL {
	case "opened":
		// FIXME: Cannot scape & because g_markup_escape_text is not binded yet
		return Opened, nil
	case "closed":
		return Closed, nil
	case "commented on":
		return CommentedOn, nil
	default:
		return "", xerrors.Errorf("WARNING: %s isn't a valid action name in an issue", actionNameGL)
	}
}

func getMRText(actionNameGL string, authorID int) (MRAction, error) {
	switch actionNameGL {
	case "accepted":
		if authorID == currentUserID {
			// FIXME: Cannot scape & because g_markup_escape_text is not binded yet
			return ReviewedAndMerged, nil
		}
		// FIXME: Cannot scape & because g_markup_escape_text is not binded yet
		return ReviewedAndMerged, nil
	case "opened":
		return Implemented, nil
	case "closed":
		return Rejected, nil
	default:
		return "", xerrors.Errorf("WARNING: %s isn't a valid action name in a MR", actionNameGL)
	}
}
