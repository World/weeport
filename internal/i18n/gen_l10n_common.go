// +build generator

package i18n

import (
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"golang.org/x/xerrors"
)

// GenerateMos generates all mo files in moPath from pos in poPath
func GenerateMos(poPath, moPath, domain string) error {
	r, err := filepath.Glob(filepath.Join(poPath, "*.po"))
	if err != nil {
		return xerrors.Errorf("couldn't list po files in %q: %v", r, err)
	}

	for _, po := range r {
		lang := strings.TrimSuffix(filepath.Base(po), ".po")
		outputDir := filepath.Join(moPath, lang, "LC_MESSAGES")
		if err := os.MkdirAll(outputDir, os.ModePerm); err != nil {
			return xerrors.Errorf("couldn't create %s: %v", outputDir, err)
		}
		if out, err := exec.Command("msgfmt", "--output-file="+filepath.Join(outputDir, domain+".mo"),
			filepath.Join(poPath, lang+".po")).CombinedOutput(); err != nil {
			return xerrors.Errorf("couldn't compile mo file for %s: %v. Command output: %s", lang, err, out)
		}
	}

	return nil
}
