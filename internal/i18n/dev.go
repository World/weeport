// +build dev,!external

package i18n

import (
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"

	"github.com/leonelquinteros/gotext"
)

//go:generate go run --tags generator gen_localization.go -po-only ../../

func init() {
	_, currentFile, _, ok := runtime.Caller(0)
	if !ok {
		log.Fatal("Couldn't find current assets file directory")
	}

	poDir := filepath.Join(filepath.Dir(currentFile), "..", "..", "po")

	var loc, poFile string
	for _, loc = range setLocales() {
		candidate := filepath.Join(poDir, loc+".po")
		if _, err := os.Stat(candidate); err != nil {
			continue
		}
		poFile = candidate
		break
	}

	if poFile == "" {
		G = gotext.Get
		GN = gotext.GetN
		return
	}

	if useCgettext == true {
		// compile po -> mo
		moData, err := exec.Command("msgfmt", "--output-file=-", poFile).CombinedOutput()
		if err != nil {
			log.Printf("couldn't compile mo file for %s: %v. Command output: %s", poFile, err, moData)
			return
		}

		localeDir, remove, err := tempMoFile(loc, domain, moData)
		if err != nil {
			log.Printf("couldn't create temporary translations for C Gettext: %v", err)
			return
		}
		// We can't remove the temporary directly after CGettext init
		TearDown = remove

		configureCGettext(localeDir, domain)
	}

	po := gotext.Po{}
	po.ParseFile(poFile)
	G = po.Get
	GN = po.GetN
}
