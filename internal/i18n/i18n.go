package i18n

/*
// #cgo LDFLAGS: -lintl
#include <libintl.h>
#include <locale.h>
#include <stdlib.h>
*/
import "C"

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"unsafe"

	"github.com/leonelquinteros/gotext"
	"golang.org/x/xerrors"
)

// This is basically the package interface matched by all implementations

var (
	// G gets translated string from current locale
	G func(str string, vars ...interface{}) string
	// GN gets translated strings from current locale, with plural form handlings
	GN func(str string, plural string, n int, vars ...interface{}) string
	// TearDown ensure we remove temporary directories created by i18n for C Gettext. It's a no-op otherwise
	TearDown = func() error { return nil }
)

const (
	domain      = "weeport"
	useCgettext = true
)

// setLocales returns the list of user env locales (and simplified ones)
// if we use C gettext, this initialize the locale and will prefer that one,
// still returning simplified ones.
func setLocales() []string {
	loc := ""
	if useCgettext {
		empty := C.CString("")
		defer C.free(unsafe.Pointer(empty))
		loc = C.GoString(C.setlocale(C.LC_ALL, empty))
	} else {
		// look up env variables ourselves
		for _, env := range []string{"LANGUAGE", "LC_ALL", "LC_MESSAGES", "LANG"} {
			if val := os.Getenv(env); val != "" {
				if val == "C" || val == "POSIX" {
					break
				}

				loc = val
				break
			}
		}
	}

	if loc == "" {
		return nil
	}

	r := []string{gotext.SimplifiedLocale(loc)}
	// treat en_US appending en as a fallback
	if len(loc) > 2 {
		r = append(r, gotext.SimplifiedLocale(loc[:2]))
	}

	return r
}

func configureCGettext(dir, domain string) {
	if !useCgettext || dir == "" {
		return
	}
	cDir := C.CString(dir)
	defer C.free(unsafe.Pointer(cDir))

	cDomain := C.CString(domain)
	defer C.free(unsafe.Pointer(cDomain))

	C.GoString(C.bindtextdomain(cDomain, cDir))
	C.GoString(C.textdomain(cDomain))
}

func tempMoFile(loc, domain string, data []byte) (localeDir string, cleanup func() error, err error) {
	localeDir, err = ioutil.TempDir("", "weeport-locale")
	if err != nil {
		return "", nil, xerrors.Errorf("couldn't create temporary locale directory: %v", err)
	}
	cleanup = func() error { return os.RemoveAll(localeDir) }

	moFile := getMoPath(localeDir, loc, domain)
	if err = os.MkdirAll(filepath.Dir(moFile), os.ModePerm); err != nil {
		cleanup()
		return "", nil, xerrors.Errorf("couldn't create temporary locale directory structure: %v", err)
	}

	if err := ioutil.WriteFile(moFile, data, 0666); err != nil {
		cleanup()
		return "", nil, xerrors.Errorf("couldn't write temporary translation file to: %v", err)
	}

	return localeDir, cleanup, nil
}

func getMoPath(localeDir, loc, domain string) string {
	return filepath.Join(localeDir, loc, "LC_MESSAGES", domain+".mo")
}
