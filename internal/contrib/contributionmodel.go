package contrib

import (
	"fmt"
	"sort"
	"time"

	"golang.org/x/xerrors"
)

// Contribution is a contribution element for a give project/target/action
type Contribution struct {
	Title       string
	Link        string
	Description string
	ID          int
	AuthorID    int

	GLProjectName string
	GLTargetType  string
	GLActionName  string
	GLCreatedAt   time.Time

	ProjectName string
	TargetType  string
	ActionName  string

	ReportTitle       string
	ReportDescription string
}

func (i Contribution) String() string {
	return fmt.Sprintf(`ProjectName: %s
TargetType: %s
ActionName: %s

Title: %s
ReportTitle: %s
Link: %s
ID: %d
AuthorID: %d
CreatedAt: %v
ReportDescription:|

%s
`,
		i.GLProjectName, i.GLTargetType, i.ActionName,
		i.Title, i.ReportTitle, i.Link, i.ID, i.AuthorID,
		i.GLCreatedAt, i.ReportDescription)
}

// Section is a recursive data that holds both contributions and other sections
type Section struct {
	Title         string
	Subsections   []*Section
	Contributions []*Contribution
}

// Model is the main contribution element, contains all sections and contributions in a wrapping recursive manner
type Model struct {
	Sections []*Section
}

// NewModel creates a new Model from given existing contributions fetched from GitLab
func NewModel(contribs []Contribution) *Model {
	cm := Model{}
	if len(contribs) == 0 {
		return &cm
	}

	By(projectTargetActionDateSort).sort(contribs)

	var curSection, curSubsection *Section
	var curProject, curAction, prevProject, prevAction string
	for _, c := range contribs {
		// Make sure to shadow the iterator pointer
		c := c
		prevProject = curProject
		prevAction = curAction
		curProject = c.ProjectName
		curAction = c.ActionName
		if curProject != prevProject {
			curSection = &Section{Title: curProject}
			curSubsection = &Section{Title: curAction}
			curSection.Subsections = append(curSection.Subsections, curSubsection)
			cm.Sections = append(cm.Sections, curSection)
		} else if curAction != prevAction {
			curSubsection = &Section{Title: curAction}
			curSection.Subsections = append(curSection.Subsections, curSubsection)
		}
		curSubsection.Contributions = append(curSubsection.Contributions, &c)
	}

	return &cm
}

// Append inserts contribution to model in the last position of the subsection
func (cm *Model) Append(c Contribution) error {
	for _, s := range cm.Sections {
		for _, ss := range s.Subsections {
			if s.Title == c.ProjectName && ss.Title == c.ActionName {
				ss.Contributions = append(ss.Contributions, &c)
				return nil
			}
		}
	}

	return xerrors.Errorf("Couldn't append item, project or section not found")
}

// Remove removes contribution from model
func (cm *Model) Remove(c *Contribution) bool {
	for _, s := range cm.Sections {
		for _, ss := range s.Subsections {
			for ci, curContrib := range ss.Contributions {
				if curContrib.ID == c.ID {
					ss.Contributions = append(ss.Contributions[:ci], ss.Contributions[ci+1:]...)
					return true
				}
			}
		}
	}

	return false
}

// RemoveFull removes contribution from model plus empty parent sections
func (cm *Model) RemoveFull(c *Contribution) bool {
	removed := false
	for si, s := range cm.Sections {
		for ssi, ss := range s.Subsections {
			if s.Title == c.ProjectName && ss.Title == c.ActionName {
				for ci, curContrib := range ss.Contributions {
					if curContrib.ID == c.ID {
						ss.Contributions = append(ss.Contributions[:ci], ss.Contributions[ci+1:]...)
						removed = true
						break
					}
				}
			}
			if len(ss.Contributions) == 0 && len(ss.Subsections) == 0 {
				s.Subsections = append(s.Subsections[:ssi], s.Subsections[ssi+1:]...)
			}
			if removed {
				break
			}
		}
		if len(s.Contributions) == 0 && len(s.Subsections) == 0 {
			cm.Sections = append(cm.Sections[:si], cm.Sections[si+1:]...)
		}
		if removed {
			break
		}
	}

	return removed
}

// MoveUp moves the contribution one position up
func (cm *Model) MoveUp(c *Contribution) {
	for _, s := range cm.Sections {
		for _, ss := range s.Subsections {
			if s.Title == c.ProjectName && ss.Title == c.ActionName {
				for ci, curContrib := range ss.Contributions {
					if curContrib.ID == c.ID && ci != 0 {
						prev := ss.Contributions[ci-1]
						ss.Contributions[ci-1] = c
						ss.Contributions[ci] = prev
						return
					}
				}
			}
		}
	}
}

// MoveDown moves the contribution one position down
func (cm *Model) MoveDown(c *Contribution) {
	for _, s := range cm.Sections {
		for _, ss := range s.Subsections {
			if s.Title == c.ProjectName && ss.Title == c.ActionName {
				for ci, curContrib := range ss.Contributions {
					if curContrib.ID == c.ID && ci < (len(ss.Contributions)-1) {
						next := ss.Contributions[ci+1]
						ss.Contributions[ci+1] = c
						ss.Contributions[ci] = next
						return
					}
				}
			}
		}
	}
}

// Contributions retrieves contributions in breath first order,
// and sections being first before section contributions
func (cm *Model) Contributions() []*Contribution {
	var result []*Contribution
	for _, s := range cm.Sections {
		result = append(result, contributions(s)...)
	}

	return result
}

func contributions(s *Section) []*Contribution {
	var result []*Contribution
	// First sections, then our own contributions
	for _, s := range s.Subsections {
		result = append(result, contributions(s)...)
	}
	for _, c := range s.Contributions {

		result = append(result, c)
	}

	return result
}

// By is the type of a "less" function that defines the ordering of its arguments.
type By func(c1, c2 *Contribution) bool

// contributionsSorter joins a By function and a slice of Contributions to be sorted.
type contributionsSorter struct {
	contributions []Contribution
	by            func(c1, c2 *Contribution) bool // Closure used in the Less method.
}

// Len is part of sort.Interface.
func (c *contributionsSorter) Len() int {
	return len(c.contributions)
}

// Swap is part of sort.Interface.
func (c *contributionsSorter) Swap(i, j int) {
	c.contributions[i], c.contributions[j] = c.contributions[j], c.contributions[i]
}

// Less is part of sort.Interface. It is implemented by calling the "by" closure in the sorter.
func (c *contributionsSorter) Less(i, j int) bool {
	return c.by(&c.contributions[i], &c.contributions[j])
}

func (by By) sort(contributions []Contribution) {
	cs := &contributionsSorter{
		contributions: contributions,
		by:            by, // The Sort method's receiver is the function (closure) that defines the sort order.
	}
	sort.Sort(cs)
}

// Closures that order the Statistic structure.
func projectTargetActionDateSort(c1, c2 *Contribution) bool {
	if c1.ProjectName != c2.ProjectName {
		return c1.ProjectName > c2.ProjectName
	}
	if c1.TargetType != c2.TargetType {
		return c1.TargetType > c2.TargetType
	}
	if c1.ActionName != c2.ActionName {
		return c1.ActionName > c2.ActionName
	}

	return c1.GLCreatedAt.Before(c2.GLCreatedAt)
}
