package contrib

import (
	"bytes"
	"encoding/gob"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/gotk3/gotk3/glib"
	"golang.org/x/xerrors"
)

// SaveToDisk saves model to disk
func (m Model) SaveToDisk() error {
	b := new(bytes.Buffer)

	e := gob.NewEncoder(b)

	// Encoding the map
	err := e.Encode(m)
	if err != nil {
		panic(err)
	}

	p := getLocalDataFilePath()
	if err := os.MkdirAll(filepath.Dir(p), os.ModePerm); err != nil {
		return xerrors.Errorf("couldn't create data path directory: %v", err)
	}

	if err := ioutil.WriteFile(p, b.Bytes(), 0666); err != nil {
		return xerrors.Errorf("couldn't write to token to file: %v", err)
	}
	return nil
}

// NewFromDisk returns a new model locally saved
func NewFromDisk() (*Model, error) {
	b, err := os.Open(getLocalDataFilePath())
	if err != nil {
		return nil, xerrors.Errorf("couldn't open local data file: %v", err)
	}
	var decodedModel Model
	d := gob.NewDecoder(b)

	err = d.Decode(&decodedModel)
	if err != nil {
		return nil, xerrors.Errorf("couldn't decode model: %v", err)
	}

	return &decodedModel, nil
}

func getLocalDataFilePath() string {
	dataPath := filepath.Join(glib.GetUserDataDir(), "weeport")
	return filepath.Join(dataPath, "data.txt")
}
