package contrib

import (
	"io"
	"text/template"

	"golang.org/x/xerrors"
)

type generator interface {
	header() string
	projectTemplate() string
	actionTemplate() string
	contributionTemplate() string
	sectionEnd() string
	end() string
}

// Generate report formatted by generator on current Model.
func (model *Model) Generate(generator generator, out io.Writer) error {
	out.Write([]byte(generator.header()))

	for _, s := range model.Sections {
		project := s.Title
		t := template.Must(template.New("project").Parse(generator.projectTemplate()))
		if err := t.Execute(out, project); err != nil {
			return xerrors.Errorf("invalid project object given to template: %v", err)
		}
		for _, ss := range s.Subsections {
			action := ss.Title
			if len(ss.Contributions) == 0 {
				continue
			}

			t = template.Must(template.New("action").Parse(generator.actionTemplate()))
			if err := t.Execute(out, action); err != nil {
				return xerrors.Errorf("invalid action object given to template: %v", err)
			}
			for _, c := range ss.Contributions {
				t = template.Must(template.New("item").Parse(generator.contributionTemplate()))
				if err := t.Execute(out, c); err != nil {
					return xerrors.Errorf("invalid item object given to template: %v", err)
				}
			}
			out.Write([]byte(generator.sectionEnd()))
		}
		out.Write([]byte(generator.sectionEnd()))
	}
	out.Write([]byte(generator.end()))
	return nil
}
