package builder

import (
	"io"
	"io/ioutil"

	"github.com/gotk3/gotk3/gtk"
	"golang.org/x/xerrors"
)

//go:generate go run gen_types.go

// Builder have helpers functions to retrieves typed object from an ui file
type Builder struct {
	*gtk.Builder
}

// New instantiate a new builder to access elements from io.Reader ui file
func New(r io.Reader) (*Builder, error) {
	gtkBuilder, err := gtk.BuilderNew()
	if err != nil {
		return nil, xerrors.Errorf("can't create a gtk builder: %v", err)
	}

	b, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, xerrors.Errorf("couldn't read from reader: %v", err)
	}

	if err = gtkBuilder.AddFromString(string(b)); err != nil {
		return nil, xerrors.Errorf("invalid UI content: %v", err)
	}

	return &Builder{gtkBuilder}, nil
}
