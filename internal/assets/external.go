// +build external

package assets

import (
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

//go:generate go run install_data.go -destdir=$DESTDIR -prefix=$PREFIX data/ share/weeport/

const dest = "weeport"

// Data contains data assets installed on disk
var Data http.FileSystem

func init() {
	p := ""
	for _, datadir := range getDataDirs() {
		candidate := filepath.Join(datadir, dest)
		info, err := os.Stat(candidate)
		if err != nil || !info.IsDir() {
			continue
		}
		p = candidate
	}
	if p == "" {
		log.Fatalln("Couldn't find 'weeport' data dirs in", getDataDirs())
	}

	Data = http.Dir(p)
}

func getDataDirs() (paths []string) {
	systemDataDirs := strings.Split(os.Getenv("XDG_DATA_DIRS"), ":")
	// we always append /usr/local/share and /usr/share as some platforms don't include them in XDG_DATA_DIRS
	for _, p := range append(systemDataDirs, "/usr/local/share", "/usr/share") {
		// Ignore non absolute paths
		if !filepath.IsAbs(p) {
			continue
		}
		paths = append(paths, p)
	}
	return paths
}
