// +build dev

package assets

import (
	"log"
	"net/http"
	"path/filepath"
	"runtime"
)

// Data contains data assets on disk for live iterations
var Data http.FileSystem

func init() {
	_, currentFile, _, ok := runtime.Caller(0)
	if !ok {
		log.Fatal("Couldn't find current assets file directory")
	}
	Data = http.Dir(filepath.Join(filepath.Dir(currentFile), "data"))
}
