package gencode

import (
	"os"
	"regexp"

	"golang.org/x/xerrors"
)

// Generator defines a standard Generate in dest behavior
type Generator interface {
	Generate(dest string) error
}

// GenCode is the struct used to defined Generation, checking and cleanup
type GenCode struct {
	Generator

	Dest      string
	destIsDir bool

	CheckMode            bool
	Verbose              bool
	IgnoreInCheckFilters []*regexp.Regexp
	FilesToIgnore        []string

	Close func()
}

// GenerateAndCheck will generate from Generator, and optionally check if the generation is fine.
// Output should be cleaned up by using Close()
func (g *GenCode) GenerateAndCheck() error {
	dest := g.Dest

	info, err := os.Stat(dest)
	if err != nil {
		return xerrors.Errorf("%q doesn't exist", err)
	}
	g.destIsDir = info.IsDir()

	// Ensure we can always defer Close
	if g.Close == nil {
		g.Close = func() {}
	}

	if g.CheckMode {
		tmp, removeTemp, err := CreateTemp(g.Dest, g.destIsDir)
		if err != nil {
			return xerrors.Errorf("couldn't create temporary content: %v", err)
		}
		g.Close = removeTemp
		dest = tmp
	}

	if err := g.Generate(dest); err != nil {
		g.Close()
		return err
	}

	// Now compare the content
	if g.CheckMode {
		identical, err := g.same(dest)
		if err != nil {
			g.Close()
			return err
		}
		if !identical {
			g.Close()
			return xerrors.New("In tree and generated files differs")
		}
	}
	return nil
}
